import React, {useEffect} from 'react';
import { PostData } from '../types';
import { useDispatch, useSelector } from 'react-redux';
import { getPost } from './homeSlice';
import { Link } from 'react-router-dom';
const Home: React.FC =(): JSX.Element =>{
  const {posts} = useSelector((state: any) => state.post);
  const dispatch: any = useDispatch()
  useEffect(() => {
    dispatch(getPost())
  }, [])
  console.log(Link)
    return(
      <div className='list-home'> 
        <div className='title'>
          Danh sách  
        </div>
        <div className='list-home-1'>
        {posts && posts.length > 0 && posts.map((post: PostData ) => {
          return(
            <div className='child' key={post.id} >
              <a href={`/${post.id}`}>  
                {post.id}-{post.title}
              </a>
            </div>
          )}
        )}
        </div>
      </div>
    )
  }

export default Home;

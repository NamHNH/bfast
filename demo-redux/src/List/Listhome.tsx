import React, {useEffect} from "react";
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from "react-router-dom";
import { PostData } from "../types";
import { getPostById } from "./homeSlice";
// In order to implement that, I'd apply my type to the hook when calling it.

type QuizParams = {
    id:  any;
  };
const Listhome:React.FC  = ()=>{
  let detailposts: PostData = useSelector((state: any) => state.post.detailposts);
  const { id } = useParams<QuizParams>();
  const dispatch: any = useDispatch()
  useEffect(() => {
    dispatch(getPostById(id))
  }, [id])

     return(
        <div className='list-home'>
        <div className='title'>
          Danh sách Chi Tiết 
        </div>
        <div className='list-home-1'>        
        {
          detailposts && (
            <div className='child' key={detailposts.id} >
                <div>ID: {detailposts.id}</div>
                <div>Tile: {detailposts.title}</div>
                <div>Body: {detailposts.body}</div>
                <a href='/'>Back</a>
            </div>  
          )}
        </div>
      </div>
     )
    }
export default Listhome;
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";

type PostState = {
  posts: [];
  detailposts: {}
};

const initialState: PostState = {
  posts: [],
  detailposts: {}
};
 
const PostSlice = createSlice({
  name: "post",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getPost.pending, (state, action) => {});
    builder.addCase(getPost.fulfilled, (state, action) => {
      state.posts = action.payload;
    });
    builder.addCase(getPost.rejected, (state, action) => {});
    builder.addCase(getPostById.pending, (state, action) => {});
    builder.addCase(getPostById.fulfilled, (state, action) => {
      state.detailposts = action.payload;
    });
    builder.addCase(getPostById.rejected, (state, action) => {});
  },
});

/////// API //////
export const getPost = createAsyncThunk(
  "post/getPost",
  async ( _: void,{ rejectWithValue }: any) => {
    try {
      let res = await axios.get("https://jsonplaceholder.typicode.com/posts");
      return res.data;
    } catch (err: any) {
      return rejectWithValue(err.response.data);
    }
  }
);
export const getPostById = createAsyncThunk(
  "post/getPostById",
  async ( id: number,{ rejectWithValue }: any) => {
    try {
      let res = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
      return res.data;
    } catch (err: any) {
      return rejectWithValue(err.response.data);
    }
  }
);

export default PostSlice.reducer;


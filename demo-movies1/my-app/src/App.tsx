
import {
  BrowserRouter ,
  Routes,
  Route,
  
} from "react-router-dom";
import './App.scss';
import Header from './components/Header/Header';
import Home from './components/Home/Home';
import MovieDetail from './components/MovieDetail/MovieDetail';
import PageNotFound from './components/PageNotFound/PageNotFound';
import Footer from './components/Footer/Footer';
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { getAllMovies, Top_Rated } from './features/movies/movieSlice';
import Login from "./components/Login/Login";
function App() {
  const dispatch = useDispatch<any>() ;
  const [user, setUser] = useState<any>("");

  useEffect(() => {
     const users: any = localStorage.getItem("user");
    if (users) {
      setUser(JSON.parse(users));
    }
  }, []);

  useEffect(() => {
    dispatch(getAllMovies());
    dispatch(Top_Rated());
  }, [dispatch]);
  
  return (
    <div className='app'>
        <BrowserRouter>
            <Header username={user?.username}/>
            <Routes>
            <Route path="/"  element={<Home/>}/>
            <Route path="/movie/:id" element={<MovieDetail/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route element={<PageNotFound/>}/>
            </Routes>
            <Footer/>
        </BrowserRouter>
    </div>
  );
}

export default App;

import axios from "axios";
import React, { useState } from "react";
import ReactDOM from "react-dom";
import { useNavigate } from "react-router-dom";

import "./Login.scss";

function Login() {
  // React States
  const [errorMessages, setErrorMessages] = useState<any>({
    name: "",
    message: "",
  });
  const [valueForm, setValueForm] = useState<any>({
    username: "",
    password: "",
  });
  const [isSubmitted, setIsSubmitted] = useState(false);
  const navigate = useNavigate();
  var { username, password } = valueForm;
  // User Login info
  const database = [
    {
      username: "1",
      password: "1",
    },
  ];

  const errors = {
    message: "Wrong account or password. Please re-enter!",
  };

  const handleSubmit = (event: any) => {
    //Prevent page reload
    event.preventDefault();

    if (!username) {
      setErrorMessages({ message: "Please enter the username" });
      return;
    }
    if (!password) {
      setErrorMessages({ message: "Please enter the password" });
      return;
    }
    // Find user login info
    const userData = database.find(
      (user) => user.username === username && user.password === password
    );

    // Compare user info
    if (!userData) {
      // Username not found
      setErrorMessages({ name: "user not found", message: errors.message });
      return;
    }
    navigate("/");
    localStorage.setItem("user", JSON.stringify(userData));
  };

  // JSX code for login form
  const handleOnChangeInput = (e: any) => {
    setValueForm({ ...valueForm, [e.target.name]: e.target.value });
  };
  const renderForm = (
    <div className="form">
      <form onSubmit={handleSubmit}>
        <div>{errorMessages?.message}</div>
        <div className="input-container">
          <label>Username </label>
          <input
            type="text"
            name="username"
            value={username}
            onChange={handleOnChangeInput}
            required
          />
        </div>
        <div className="input-container">
          <label>Password </label>
          <input
            type="password"
            name="password"
            value={password}
            onChange={handleOnChangeInput}
            required
          />
        </div>
        <div className="button-container">
          <input type="submit" />
        </div>
      </form>
    </div>
  );

  return (
    <div className="login">
      <div className="login-form">
        <div className="title">Sign In</div>
        {isSubmitted ? <div>User is successfully logged in</div> : renderForm}
      </div>
    </div>
  );
}

export default Login;

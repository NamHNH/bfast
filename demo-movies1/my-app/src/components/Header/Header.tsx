import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import user from "../../images/user.png";
import "./Header.scss";
const Header = ({ username }: any) => {
  const navigate = useNavigate();
  const handleLogout = () => {
    localStorage.clear();
    navigate("/login");
  };
  return (
    <div className="header">
      <Link to="/">
        <div className="logo">Movie App</div>
      </Link>
      <div>
        <span>{username}</span>
        <div className="user-image">
          <Link to="/login">
            <img src={user} alt="user" />
          </Link>
        </div>
        {username ? <button onClick={handleLogout}></button> : ""}
      </div>
    </div>
  );
};
export default Header;

import React from "react";
import { useNavigate } from "react-router-dom";
import { IMGPATH } from "../../ultilitis/constans";
import "./MovieCard.scss";

const MovieCard = ({ movie }: any) => {
  // console.log(movie);
  const history = useNavigate();

  return (
    <>
      <div className="card-item" key={movie.id}>
        <div className="card-inner">
          <div className="card-top">
            <img src={IMGPATH + movie.poster_path} alt={movie.title} />
          </div>
          <div className="card-bottom">
            <h4>{movie.title}</h4>
            <p>{movie.release_data}</p>
          </div>
        </div>
      </div>
    </>
  );
};

export default MovieCard;

import React, { useEffect } from "react";
import movieApi from "../../common/api/movieApi";
import { useDispatch } from "react-redux";
import { addMovies, getAllMovies } from "../../features/movies/movieSlice";
import { APIKey } from "../../common/api/MovieApiKey";
import MovieListing from "../MovieListing/MovieListing";
import Login from "../Login/Login";

const Home = () => {
  return (
    <div>
      <div className="banner-img"></div>
      <MovieListing />
    </div>
  );
};

export default Home;

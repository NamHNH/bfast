import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { SwiperSlide, Swiper } from "swiper/react";
import "./MovieListing.scss";
import Slider from "react-slick";
import MovieCard from "../MovieCard/MovieCard";
const MovieListing: React.FC = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 2,
    arrows: true,
  };

  const { movies, tvShow } = useSelector((state: any) => state.movies);

  return (
    <div className="movies-wrapper">
      <div className="movie-list">
        <h1>Movies</h1>
        <div className="movie-containners">
          <Slider {...settings}>
            {movies?.map((movie: any) => (
              <Link to={`/movie/${movie.id}`}>
                <MovieCard key={movie.id} movie={movie} />
              </Link>
            ))}
          </Slider>
        </div>

        <h1>TV Show</h1>
        <div className="movi-containners">
          <Slider {...settings}>
            {tvShow?.map((movie: any) => (
              <Link to={`/movie/${movie.id}`}>
                <MovieCard key={movie.id} movie={movie} />
              </Link>
            ))}
          </Slider>
        </div>
      </div>
    </div>
  );
};

export default MovieListing;

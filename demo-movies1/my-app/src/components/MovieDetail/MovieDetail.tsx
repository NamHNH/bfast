import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { movieDetail, castsDetail } from "../../features/movies/movieSlice";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./MovieDetail.scss";
const MovieDetail = () => {
  const { movie, casts } = useSelector((state: any) => state.movies);

  const dispatch = useDispatch<any>();
  const { id } = useParams<any>();
  const IMGPATH = "https://image.tmdb.org/t/p/w1280";
  useEffect(() => {
    dispatch(movieDetail(id));
    dispatch(castsDetail(id));
  }, [id, dispatch]);
  console.log({ casts });
  var settings = {
    dots: true,
    // infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 2,
  };
  console.log(movie, casts);
  return (
    <div className="card-toop" style={{ color: "white" }}>
      <div className="card-left">
        <img src={IMGPATH + movie.poster_path} alt={movie.title} height="500" />
        <div>{movie?.title}</div>
        <div>
          <div> Mô Tả: {movie?.overview}</div>
        </div>
      </div>
      <div className="card-right">
        <Slider {...settings}>
          {casts?.cast?.map((cast: any) => (
            <div key={cast.cast_id}>
              <img
                src={IMGPATH + cast.profile_path}
                alt={cast.name}
                title={cast.name}
              />
              <span> {cast.name} </span>
            </div>
          ))}
        </Slider>
      </div>
    </div>
  );
};

export default MovieDetail;

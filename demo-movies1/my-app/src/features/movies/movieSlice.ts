import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { APIKey } from "../../common/api/MovieApiKey";
import movieApi from "../../common/api/movieApi";
import axios from "axios";
const API_Key: string = "e9e9d8da18ae29fc430845952232787c";
const Top_rated: string ="https://api.themoviedb.org/3/movie/top_rated?api_key=e9e9d8da18ae29fc430845952232787c&language=en-US&page=1";
const initialState = {
  movies: [],
  movie: {},
  casts: [],
  tvShow: [],
};
export const getAllMovies = createAsyncThunk(
  "movies/getAllMovies",
  async () => {
    const response = await movieApi.get(`movie?api_key=${APIKey}`);
    return response.data.results;
  }
);
export const movieDetail = createAsyncThunk(
  "movies/movieDetail",
  async (id: any) => {
    const response = await movieApi.get(
      `https://api.themoviedb.org/3/movie/${id}?api_key=${API_Key}&append_to_response=videos`
    );
    return response.data;
  }
);
export const castsDetail: any = createAsyncThunk(
  "movies/moviecCasts",
  async (id: any) => {
    const responses = await movieApi.get(
      `http://api.themoviedb.org/3/movie/${id}/casts?api_key=${API_Key}`
    );
    return responses.data;
  }
);
export const Top_Rated: any = createAsyncThunk(
  "movies/Top_rated",
  async (id: any) => {
    const responses = await axios.get(`${Top_rated}`);
    return responses.data.results;
  }
);

const movieSlice = createSlice({
  name: "movies",
  initialState,
  reducers: {
    addMovies: (state, action) => {
      state.movies = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getAllMovies.pending, (state, action) => {});
    builder.addCase(getAllMovies.fulfilled, (state, action) => {
      state.movies = action.payload;
    });
    builder.addCase(getAllMovies.rejected, (state, action) => {});
    builder.addCase(movieDetail.pending, (state, action) => {});
    builder.addCase(movieDetail.fulfilled, (state, action) => {
      state.movie = action.payload;
    });
    builder.addCase(movieDetail.rejected, (state, action) => {});
    builder.addCase(castsDetail.pending, (state, action) => {});
    builder.addCase(castsDetail.fulfilled, (state, action) => {
      state.casts = action.payload;
    });
    builder.addCase(castsDetail.rejected, (state, action) => {});
    builder.addCase(Top_Rated.pending, (state, action) => {});
    builder.addCase(Top_Rated.fulfilled, (state, action) => {
      state.tvShow = action.payload;
    });
    builder.addCase(Top_Rated.rejected, (state, action) => {});
  },
});

export const { addMovies } = movieSlice.actions;
export default movieSlice.reducer;

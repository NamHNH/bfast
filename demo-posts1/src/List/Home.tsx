import React, {useEffect} from 'react';
import { PostData } from '../types';
import { useDispatch, useSelector } from 'react-redux';
import { getPost } from './homeSlice';
import { Link } from 'react-router-dom';
const Home: React.FC =(): JSX.Element =>{
  const {Home} = useSelector((state: any) => state.post);
  const dispatch: any = useDispatch()
  useEffect(() => {
    dispatch(getPost())
  }, [])
  console.log(Link)
    return(
      <div className='list-home'> 
        <div className='title'>
          Danh sách  
        </div>
        <div className='list-home-1'>
        {Home && Home.length > 0 && Home.map((post: PostData ) => {
          return(
            <div className='child' key={post.id} >
              <a href={`/${post.id}`}>  
                {post.id}-{post.title}
              </a>
            </div>
          )}
        )}
        </div>
      </div>
    )
  }

export default Home;

import React from 'react';
import logo from './logo.svg';
import {
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import './App.css';
import Home from './List/Home';
import Listhome from './List/Listhome';

function App() {
  return (
    <div className="App">
          <img src={logo} className="App-logo" alt="logo" />
      <header className="App-header">
        <Switch>
        <Route path='/' exact>
          <Home/>
        </Route>
        <Route path='/:id'>
          <Listhome/>
        </Route>
        <Redirect to="/" />
      </Switch>
      </header>
    </div>
  );
}

export default App;
